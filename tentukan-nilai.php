<?php
	function tentukan_nilai($number)
	{
	    //  kode disini
	    if ($number >= 85 && $number <= 100) {
	    	# code...
	    	echo "Sangat Baik <br>";
	    } else if ($number >= 70 && $number < 85) {
	    	# code...
	    	echo "Baik <br>";
	    } else if ($number >= 60 && $number < 70) {
	    	# code...
	    	echo "Cukup <br>";
	    } else if ($number < 60) {
	    	# code...
	    	echo "Kurang";
	    }
	}

	//TEST CASES
	echo tentukan_nilai(98); //Sangat Baik
	echo tentukan_nilai(76); //Baik
	echo tentukan_nilai(67); //Cukup
	echo tentukan_nilai(43); //Kurang
?>