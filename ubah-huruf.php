<?php
	function ubah_huruf($string){
	//kode di sini
		$alpabet = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'];
		$plusSatu = "";
		if (!is_string($string)) {
			# code...
			return "Masukan nilai String";
		} else {
			foreach (str_split($string) as $huruf) {
				# code...
				for ($i=0; $i < count($alpabet); $i++) { 
					# code...
					if (strtolower($huruf) == $alpabet[$i]) {
						# code...
						$plusSatu .= $alpabet[$i+1];
					}
				}
			}
			return $plusSatu ."<br>";
		}

	}

	// TEST CASES
	echo ubah_huruf('wow'); // xpx
	echo ubah_huruf('developer'); // efwfmpqfs
	echo ubah_huruf('laravel'); // mbsbwfm
	echo ubah_huruf('keren'); // lfsfo
	echo ubah_huruf('semangat'); // tfnbohbu
?>