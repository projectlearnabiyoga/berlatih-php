<?php
	function tukar_besar_kecil($string){
	//kode di sini
		$tukarHuruf = "";
		foreach (str_split($string) as $huruf) {
			# code...
			if (ctype_lower($huruf)) {
				# code...
				$tukarHuruf .= strtoupper($huruf);
			} else if (ctype_upper($huruf)) {
				# code...
				$tukarHuruf .= strtolower($huruf);
			} elseif (ctype_digit($huruf)) {
				# code...
				$tukarHuruf .= $huruf;
			} elseif (ctype_print($huruf)) {
				# code...
				$tukarHuruf .= $huruf;
			}
		}
		return $tukarHuruf . "<br>";
	}

	// TEST CASES
	echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
	echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
	echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
	echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
	echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"

?>